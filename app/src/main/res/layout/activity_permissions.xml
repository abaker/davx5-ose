<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        <variable name="model" type="at.bitfire.davdroid.ui.PermissionsFragment.Model"/>
    </data>

    <ScrollView
        android:id="@+id/frame"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="?android:attr/colorBackground">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:padding="@dimen/activity_margin">

            <com.google.android.material.card.MaterialCardView
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:paddingBottom="@dimen/card_padding">

                    <at.bitfire.davdroid.ui.widget.CropImageView
                        android:id="@+id/image"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        app:layout_constraintVertical_chainStyle="packed"
                        app:layout_constraintVertical_bias="0"
                        android:maxHeight="@dimen/card_theme_max_height"
                        app:layout_constraintTop_toTopOf="parent"
                        app:layout_constraintBottom_toTopOf="@id/heading"
                        android:adjustViewBounds="true"
                        app:verticalOffsetPercent=".45"
                        app:srcCompat="@drawable/intro_permissions"/>

                    <androidx.constraintlayout.widget.Guideline
                        android:id="@+id/start"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:orientation="vertical"
                        app:layout_constraintGuide_begin="@dimen/card_padding" />
                    <androidx.constraintlayout.widget.Guideline
                        android:id="@+id/end"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:orientation="vertical"
                        app:layout_constraintGuide_end="@dimen/card_padding" />

                    <TextView
                        android:id="@+id/heading"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/image"
                        app:layout_constraintBottom_toTopOf="@id/text"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:layout_marginTop="@dimen/card_margin_title_text"
                        style="@style/TextAppearance.MaterialComponents.Headline6"
                        android:text="@string/permissions_title" />

                    <TextView
                        android:id="@+id/text"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/heading"
                        app:layout_constraintBottom_toTopOf="@id/contactsHeading"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:layout_marginTop="@dimen/card_margin_title_text"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_text" />

                    <TextView
                        android:id="@+id/allHeading"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        app:layout_constraintTop_toBottomOf="@id/text"
                        app:layout_constraintBottom_toTopOf="@id/allStatus"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/allSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_all_title" />
                    <TextView
                        android:id="@+id/allStatus"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/allHeading"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/allSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body2"
                        android:text="@{model.haveAllPermissions ? @string/permissions_all_status_on : @string/permissions_all_status_off}" />
                    <com.google.android.material.switchmaterial.SwitchMaterial
                        android:id="@+id/allSwitch"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        app:layout_constraintTop_toTopOf="@id/allHeading"
                        app:layout_constraintBottom_toBottomOf="@id/allStatus"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:clickable="@{!model.haveAllPermissions}"
                        android:checked="@={model.needAllPermissions}" />

                    <TextView
                        android:id="@+id/contactsHeading"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="16dp"
                        app:layout_constraintTop_toBottomOf="@id/allStatus"
                        app:layout_constraintBottom_toTopOf="@id/contactsStatus"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/contactsSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_contacts_title" />
                    <TextView
                        android:id="@+id/contactsStatus"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/contactsHeading"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/contactsSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body2"
                        android:text="@{model.haveContactsPermissions ? @string/permissions_contacts_status_on : @string/permissions_contacts_status_off}" />
                    <com.google.android.material.switchmaterial.SwitchMaterial
                        android:id="@+id/contactsSwitch"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        app:layout_constraintTop_toTopOf="@id/contactsHeading"
                        app:layout_constraintBottom_toBottomOf="@id/contactsStatus"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:clickable="@{!model.haveContactsPermissions}"
                        android:checked="@={model.needContactsPermissions}" />

                    <TextView
                        android:id="@+id/calendarHeading"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/card_margin_title_text"
                        app:layout_constraintTop_toBottomOf="@id/contactsStatus"
                        app:layout_constraintBottom_toTopOf="@id/calendarStatus"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/calendarSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_calendar_title" />
                    <TextView
                        android:id="@+id/calendarStatus"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/calendarHeading"
                        app:layout_constraintBottom_toTopOf="@id/tasksHeading"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/calendarSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body2"
                        android:text="@{model.haveCalendarPermissions ? @string/permissions_calendar_status_on : @string/permissions_calendar_status_off}" />
                    <com.google.android.material.switchmaterial.SwitchMaterial
                        android:id="@+id/calendarSwitch"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        app:layout_constraintTop_toTopOf="@id/calendarHeading"
                        app:layout_constraintBottom_toBottomOf="@id/calendarStatus"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:clickable="@{!model.haveCalendarPermissions}"
                        android:checked="@={model.needCalendarPermissions}" />

                    <TextView
                        android:id="@+id/tasksHeading"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/card_margin_title_text"
                        app:layout_constraintTop_toBottomOf="@id/calendarStatus"
                        app:layout_constraintBottom_toTopOf="@id/tasksStatus"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/tasksSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_tasks_title" />
                    <TextView
                        android:id="@+id/tasksStatus"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/tasksHeading"
                        app:layout_constraintStart_toEndOf="@id/start"
                        app:layout_constraintEnd_toStartOf="@id/tasksSwitch"
                        style="@style/TextAppearance.MaterialComponents.Body2"
                        android:text="@{model.haveTasksPermissions != null ? (model.haveTasksPermissions ? @string/permissions_tasks_status_on : @string/permissions_tasks_status_off) : @string/permissions_tasks_status_not_installed}" />
                    <com.google.android.material.switchmaterial.SwitchMaterial
                        android:id="@+id/tasksSwitch"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        app:layout_constraintTop_toTopOf="@id/tasksHeading"
                        app:layout_constraintBottom_toBottomOf="@id/tasksStatus"
                        app:layout_constraintEnd_toStartOf="@id/end"
                        android:enabled="@{model.haveTasksPermissions != null}"
                        android:clickable="@{!model.haveTasksPermissions}"
                        android:checked="@={model.needTasksPermissions}" />

                    <TextView
                        android:id="@+id/appSettingsHint"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintTop_toBottomOf="@id/tasksSwitch"
                        app:layout_constraintStart_toStartOf="@id/start"
                        app:layout_constraintEnd_toEndOf="@id/end"
                        android:layout_marginTop="24dp"
                        style="@style/TextAppearance.MaterialComponents.Body1"
                        android:text="@string/permissions_app_settings_hint"/>

                    <Button
                        android:id="@+id/appSettings"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="8dp"
                        app:layout_constraintTop_toBottomOf="@id/appSettingsHint"
                        app:layout_constraintStart_toStartOf="@id/start"
                        style="@style/Widget.MaterialComponents.Button.OutlinedButton"
                        android:text="@string/permissions_app_settings" />

                </androidx.constraintlayout.widget.ConstraintLayout>

            </com.google.android.material.card.MaterialCardView>

        </LinearLayout>

    </ScrollView>

</layout>